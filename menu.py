import os
from rich import print

def menu():
    os.system('clear')
    while True:
        print(" " * 20,"[bold magenta]WELCOME TO THE THUNDERDOME[/bold magenta]"," " * 20)
        print(" ")
        print("|","-" * 10,"MENU","-" * 10,"|")
        print("*" * 30)
        print("|","[1] QUIT")
        user_input = input('<>:')
        if user_input == '1':
            os.system('clear')
            break
menu()
